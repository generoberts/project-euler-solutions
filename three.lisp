(defun prime-factors (n &optional (start 2))
  "Idea is we keep divide the given number by the start number until the origin number can't be divided by the start number anymore, and we move on to the start + 1 and repeat those steps again."
  (let ((result (rem n start)))
    (cond
      ((and (= n start)
            (= 0 result))
       (cons start nil)) ; stop after we search thru all the numbers since start to n
      ((= 0 result) ; start can divie n
       (cons start (prime-factors (/ n start) start))) ; so we simply add it to the list of number that can divide n
      (t ; start can't divide n, so we just move to start + 1
       (prime-factors n (+ 1 start))))))

(defun three ()
  (reduce #'max (prime-factors 600851475143)))
