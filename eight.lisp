(defconstant the-number 7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450)

(defun multiply (numbers adjust-num &optional (product 1))
  "adjust-num is the number of number that will be multiplied together.
   This function multiply the first adjust-num numbers of the numbers (list)."
  (if (zerop adjust-num)
      product
      (multiply (cdr numbers) (1- adjust-num) (* (car numbers) product))))

(defun multiply-in-list (numbers adjust-num &optional (result-list '()))
  (if (< (length numbers) adjust-num) ; stop after there are less number that the number number we want to multiply
      result-list ; and return the list of the results that we've collected
      (multiply-in-list
       (cdr numbers) ; start with the next number, down to adjust-num times
       adjust-num ; this ajust-num is not the same as adjust-num in multiply function. adjust-num here isn't decrease everytime multiply-in-list is call
       (append result-list 
               (list (multiply numbers adjust-num)))))) ; append the result-list with the result of multiply adjacent numbers of number in numbers

(defun num-to-list (num)
  (let* ((str-num (write-to-string num)) ; turn the integer to string...
         (list-char-of-num (concatenate 'list str-num))) ; ...and turn that string to a list of chars...
    (loop for i in list-char-of-num collect (parse-integer (string i))))) ; ...and finally turn loop for all that list of chars and turn each char to corresponding integer

(defun eightht ()
  (reduce #'max (multiply-in-list (num-to-list the-number) 13)))
