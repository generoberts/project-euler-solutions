(defun get-numbers (&optional (high 999) (low 100))
  (let ((the-list '())
        (the-numbers (loop for i from high downto low collect i)))
    (dolist (x the-numbers) ; from the first number
      (dolist (y the-numbers) ; and the first number of the same list
        (let ((product (* x y))) ; multiply them
          (if (palindomp (coerce (write-to-string product) 'list)) ; if the result is a palindom number
              (setf the-list (append the-list (list (* x y)))))))) ; add it to the list
    (remove-duplicates the-list))) ; in the end, remove all the duplicated numbers

(defun palindomp (num)
  (cond
    ((null num) t)
    ((= 0 (- (length num) 1)) t)
    (t
     (and (char= (car num)
                 (car (reverse num)))
          (palindomp (subseq num 1 (- (length num) 1)))))))

(defun four ()
  (reduce #'max ; take the max values
    (get-numbers)))))) ; of these palindom numbers
