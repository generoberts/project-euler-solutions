(defun fib (a b limit)
  (if (> b limit)
      (cons a nil)
      (cons a (cons b (fib (+ a b) (+ a b b) limit)))))

(defun two ()
  (reduce #'+ (remove-if #'oddp (fib 1 2 4000000))))
