;; this question is just asking about LCM of 1, 2, ..., 20.
(defun five ()
  (reduce #'lcm (loop for i from 1 to 20 collect i)))
