(defun get-the-triples ()
  (let ((result '()))
    (dotimes (a 1000)
      (dotimes (b 1000)
        (let ((c (- 1000 a b)))
          (if (= (expt c 2)
                 (+ (expt a 2)
                    (expt b 2)))
              (setf result (append result (list (list a b c))))))))
    result))

(defun nine ()
  (let ((nums (get-the-triples)))
    (reduce #'max ; find the max value...
            (mapcar #'(lambda (x) (* (first x) ; from the sum of each element of...
                                (second x)
                                (third x)))
                    (mapcar #'identity nums))))) ; each element in the list that we got from get-the-triples
