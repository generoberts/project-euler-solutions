(defun primep (n start)
  (cond
    ((eq n start) t) ; if we try all the number below n and none can divide n expcep n and 1, then it's prime
    ((zerop (rem n start)) nil) ; start is a divisor of n, thus n isn't prime
    (t
     (primep n (1+ start))))) ; trying next divisor

(defun all-n-primes (start end &optional (result '()))
  (cond
    ((eq end (length result)) ; stop when we have n number of primes
     result)
    ((primep start 2) ; start from 2 and go on...
     (all-n-primes (1+ start) end (cons start result))) ; if it's prime, we add it to the result list and start with the next number
    (t ; not prime...
     (all-n-primes (1+ start) end result)))) ; we simply try the next number

(defun seven ()
  (first (all-n-primes 2 10001))) ; the reslut will have the lastest prime we add in the front
