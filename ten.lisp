(defun primep (number &optional (primes '()))
  (cond
    ((null primes) ; we start with no primes
     )
    (t
     (let ((p (car primes)))
       (if (zerop (rem n p)) ; p can divide n
           nil
           (primep number (cdr primes)))))))
