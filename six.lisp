;; since 1^2 + 2^2 + ... + n^2 = (1/6)n(n+1)(2n+1)
;; and 1 + 2 + ... + n = (1/2)n(n+1)
;; thus we can use these formulars instead of directly compute the qustion
(defun six (&optional (n 100))
  (let ((sum-of-squares (* (/ 1 6) n (+ n 1) (+ 1 (* 2 n))))
        (square-of-sum (expt (* (/ 1 2) n (+ n 1)) 2)))
    (- square-of-sum sum-of-squares)))
