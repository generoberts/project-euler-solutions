(princ (reduce #'+ (remove-if #'(lambda (x) (not (or (= 0 (rem x 3))
                                                     (= 0 (rem x 5)))))
                              (loop for i from 1 to 999 collect i))))
